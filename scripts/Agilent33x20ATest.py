"""Unit Test for Agilent33x20A function generator tango device server using Jython and PyUnit

Cloned from the MoneyTest.py example from http://www.bargaud.com
"""

__version__ = "1.0"
# Agilent33x20ATest.py

import sys
import os
import time
import unittest
from fr.esrf import Tango
from fr.esrf import TangoApi

MESSAGE_REGISTRY = """
 __________________________________________________________________________
|                                                                          |
| In order to successfully run this unit test example, you need to set the |
| property "python.security.respectJavaAccessibility" to "false" in the    |
| jython registry file. The default location of the jython registry file   |
| is in the installation directory of jython, at the same level than       |
| jython.jar.                                                              |
|                                                                          |
| For more information about the jython registry, visit:                   |
| - http://www.jython.org/docs/registry.html                               |
|__________________________________________________________________________|

"""

class Agilent33x20ATest(unittest.TestCase):

  
  """PyUnit version of the original JUnit package example."""

  def __init__(self, test):
    unittest.TestCase.__init__(self, test)
    self.check_registry()
    self.assertTrue = self.assert_
    self.verbose = 1


  def check_registry(self):
    """Setting python.security.respectJavaAccessibility to false allows access
    to protected and private element of a Java class"""
    if sys.registry['python.security.respectJavaAccessibility'] == 'true':
      print MESSAGE_REGISTRY
      sys.exit(0)

  def assertFalse(self, expr, msg=None):
    """Fail the test if the expression is true."""
    if expr: raise self.failureException, msg

  def setUp(self):
    # The following three lines stop and restart the server. To save time
    # they have been commented out and e assume the server is already running
    #os.system("killall Agilent33x20A")
    #os.environ["LD_LIBRARY_PATH"] = "/segfs/tango/lib/suse82"
    #os.system("(nohup /users/goetz/workspace/Agilent33x20A/Agilent33x20A seglab) &")
    self.agilent33x20a=TangoApi.DeviceProxy("//corvus:10000/seglab/agilent33x20a/1")
	#print dev.ping()
    self.info = self.agilent33x20a.info()
	#print devinfo.server_host
	# setup frequency generator to a sine function at 1 MHz
    self.agilent33x20a.set_source(Tango.DevSource.DEV)
    msg = "    setUp() setting function to sine\n"
    #if (self.verbose): sys.stderr.write(msg)
    attr_write = TangoApi.DeviceAttribute("function","sine")
    self.agilent33x20a.write_attribute(attr_write)
    attr_read = self.agilent33x20a.read_attribute("function");
    frequency_write = 1000000.0
    msg = "    setUp() setting frequency to %f \n" % frequency_write
    #if (self.verbose): sys.stderr.write(msg)
    attr_write = TangoApi.DeviceAttribute("frequency",frequency_write)
    self.agilent33x20a.write_attribute(attr_write)
 


  def testFrequency(self):
    """write_attribute("frequency") == read_attribute("frequency")"""
    if (self.verbose): sys.stderr.write("\n")
    for i in range(15):
        try:
            frequency_write = 1000000.0*(i+1)
            msg = "    testFrequency() setting frequency to %f \n" % frequency_write
            if (self.verbose): sys.stderr.write(msg)
            attr_write = TangoApi.DeviceAttribute("frequency",frequency_write)
            self.agilent33x20a.write_attribute(attr_write)
            attr_read = self.agilent33x20a.read_attribute("frequency");
            frequency_read = attr_read.extractDouble();  
            self.assertEquals(frequency_write, frequency_read)
        except Tango.DevFailed, failed:
            msg = "    testFrequency() failed because : %s\n" % failed.errors[0].desc
            sys.stderr.write(msg)
            raise failed

  def testMaxFrequency(self):
    """test write_attribute("frequency" > max_frequency)  throws an exception"""
    if (self.verbose): sys.stderr.write("\n")
    try:
        frequency_write = 16000000.0
        msg = "    testMaxFrequency() setting frequency to %f \n" % frequency_write
        if (self.verbose): sys.stderr.write(msg)
        attr_write = TangoApi.DeviceAttribute("frequency",frequency_write)
        self.agilent33x20a.write_attribute(attr_write)
        attr_read = self.agilent33x20a.read_attribute("frequency");
        frequency_read = attr_read.extractDouble();  
        self.assertEquals(frequency_write, frequency_read)
    except Tango.DevFailed, failed:
            msg = "    testMaxFrequency() correctly received an exception because : %s\n" % failed.errors[0].desc
            sys.stderr.write(msg)

  def testVoltage(self):
    """write_attribute("voltage") == read_attribute("voltage")"""
    if (self.verbose): sys.stderr.write("\n")
    for i in range(1,10):
        try:
            voltage_write = i*1.0
            msg = "    testVoltage() setting voltage to %f \n" % voltage_write
            if (self.verbose): sys.stderr.write(msg)
            attr_write = TangoApi.DeviceAttribute("voltage",voltage_write)
            self.agilent33x20a.write_attribute(attr_write)
            attr_read = self.agilent33x20a.read_attribute("voltage");
            voltage_read = attr_read.extractDouble();  
            self.assertEquals(voltage_write, voltage_read)
        except Tango.DevFailed, failed:
            msg = "    testVoltage() failed because : %s\n" % failed.errors[0].desc
            sys.stderr.write(msg)
            raise failed

  def testFunction(self):
    """write_attribute("function") == read_attribute("function")"""
    if (self.verbose): sys.stderr.write("\n")
    functions = ["sine", "square", "triangle"]
    for i_function in functions:
        try:
            if (i_function == "triangle"):
                frequency_write = 50000.0
                msg = "    testVoltage() setting frequency to %s \n" % frequency_write
                if (self.verbose): sys.stderr.write(msg)
                attr_write = TangoApi.DeviceAttribute("frequency",frequency_write)
                self.agilent33x20a.write_attribute(attr_write)
            msg = "    testVoltage() setting function to %s \n" % i_function
            if (self.verbose): sys.stderr.write(msg)
            attr_write = TangoApi.DeviceAttribute("function",i_function)
            self.agilent33x20a.write_attribute(attr_write)
            attr_read = self.agilent33x20a.read_attribute("function");
            function_read = attr_read.extractString();  
            self.assertEquals(i_function, function_read)
        except Tango.DevFailed, failed:
            msg = "    testFunction() failed because : %s\n" % failed.errors[0].desc
            sys.stderr.write(msg)
            raise failed

def suite():
  return unittest.makeSuite(Agilent33x20ATest)

if __name__ == '__main__':
  unittest.main()

